# Ketty

Ketty is an online single source book production platform.

Ketty is being developed by [Coko](https://coko.foundation/) for you! 100% Open Source.

Visit the project's [website](https://ketty.community/) or our [chat channel](https://mattermost.coko.foundation/coko/channels/ketty).

## Project roadmap

Take a look at [the Ketty project roadmap](https://docs.ketty.community/docs/roadmap/ketty-roadmap) to see what we're working on.

## Contribute

Contributions are welcome! Please read our [Collaboration Guidelines](https://forum.ketty.community/t/community-collaboration-on-ketty/24) and join our [chat channel](https://mattermost.coko.foundation/coko/channels/ketty).

## Get help or report a bug

If you're stuck or want to report a bug, you can reach out to us at the [Ketty Help Forum](https://forum.ketty.community/) or [open a GitLab issue](https://gitlab.coko.foundation/ketty/ketty/-/issues/new) and use the bug-report template contained in the issue.

## Install and deploy

Refer to the [Ketty Docs](https://docs.ketty.community/)
