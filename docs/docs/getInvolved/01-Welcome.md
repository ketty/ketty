At Ketty, we’re building a vibrant open-source community to revolutionise book publishing. This guide is designed to help you contribute to the Ketty project, whether you’re a designer, product manager, editor, translator, system administrator, or a Ketty user. No matter your skills, experience, or the nature of your contribution, we welcome you.

:::info
This guide is for contributions beyond development. If you're looking for information on contributing code, refer to
the [Ketty Developer Guide](../developerGuide/01-Repositories%20&%20Setup.md).
:::

We understand that **Ketty users** are often the closest to the platform, and your contributions—whether through feature requests, bug reports, or updates to our documentation—are vital in shaping Ketty to meet real-world needs. Your feedback helps us build a product that is intuitive, fit-for-purpose, and user-friendly.

This guide provides structured information on how you can get involved. Below is an overview of what you can expect for:

- **Ketty users:** Information on keeping updated on our roadmap and instructions on how to submit feature requests and report bugs.
- **Editors:** Details on reviewing and editing UI text and ensuring consistency.
- **Translators:** A guide on how to translate the interface and suggest new languages.
- **QA Engineers:** Information on manual and automated testing with Cypress.

We’re continually working on expanding this guide, so if you don’t see detailed instructions for your role yet, let us know at ketty@coko.foundation.

Thank you for being part of the Ketty community. Your contributions—large or small—help us build a better open-sourse publshing ecosystem, together.
