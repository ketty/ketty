---
title: "Collaboration"
---

## Share your book

Only the Book Owner can manage book collaborators. To share your book:

1. Click "Share" in the top-right of the **Producer** page
2. Enter the collaborator's email address
3. If the person has already signed up to Ketty:
   - Select their name from the dropdown
4. If the person is new to Ketty:
   - Select the invite option
5. Choose their access level:
   - Can edit
   - Can view
6. Select "Share" to send the invitation

The Book Owner can modify or remove access at any time using the dropdown next to each collaborator's name.

## Access levels

### Edit access capabilities

Collaborators with edit access can:
- Edit book content and metadata
- Create and respond to comments
- View PDF and web previews
- Save publishing profiles
- Download PDF or EPUB files
- Use enabled AI features

### View access capabilities

Collaborators with view access can:
- View book content and metadata
- View PDF and web previews
- View publishing profiles

## Collaborative editing

Ketty uses asynchronous editing at the chapter level for controlled collaboration.

### How it works

- One collaborator can edit a chapter at a time
- Other collaborators can view chapters being edited
- Other collaborators can make and respond to comments
- Chapter locks show editor's initials next to chapter title
- Locks automatically release after 24 hours of inactivity

### Managing chapter locks

To release a chapter lock:
- Click the chapter title showing your initials to deselect it
- Or close the browser tab with the chapter open

Either action makes the chapter available for other collaborators with edit access.

### Best practices

- Regularly release chapter locks when finished editing
- Communicate with team members about editing schedules
- Use comments for feedback while chapters are locked
