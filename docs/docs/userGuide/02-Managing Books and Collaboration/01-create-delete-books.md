---
title: 'Creating and Deleting Books'
---

## Create a book

From the **Dashboard**, select "New Book". There are two ways you can add content to a new book:

### 1. Write from scratch

1. Type in your title
2. Click "Continue"
3. You'll land on the **Producer** page with a new chapter in view
4. The title can be edited later in the book's metadata

### 2. Upload your files

1. Select "Select files" to open the **Import** page
2. Upload one or more Word docx files (each file will become a separate chapter in your book)
3. Give your book a title
4. You'll land on the **Producer** page
5. Your uploaded files will appear in the book body list once they finish processing

## Delete a book

Only the Book Owner (the user who created the book) can delete a book:

1. On the **Dashboard**, locate the book you want to delete
2. Click the three dots next to the book's title
3. Select "Delete"

:::warning
This action cannot be undone.
:::
