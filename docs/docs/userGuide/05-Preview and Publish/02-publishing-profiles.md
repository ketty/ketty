---
title: 'Publishing Profiles'
---

## Overview

Once you've saved a preview you can access it as a publishing profile. Use publishing profiles to: 

1. Export PDF and EPUB profiles for distribution with external suppliers
2. Connent PDF and EPUB profiles with your Lulu.com account for distribution with their print-on-demand services
3. Publish your book online

## Accessing saved profiles

1. Navigate to the **Preview and Publish** page
2. Select the "Publishing Profiles" tab
3. Choose a profile from the dropdown menu

## Working with profiles

Each saved profile allows you to:
- Update existing settings
- Rename the profile
- View profile information such as the last updated date
- Delete the profile if no longer needed
