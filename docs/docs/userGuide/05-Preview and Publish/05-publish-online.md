---
title: 'Publish Online'
---

## Publish online with Flax

Ketty integrates with Flax, a static site generation tool, through the publishing profiles. 

1. Navigate to the **Preview and Publish** page
2. Select the "Publishing Profiles" tab
3. Choose a web profile from the dropdown menu

:::note
Only Book Owners can publish a book online.
:::

## Initial publishing

After selecting the relevant profile: 
1. Select "Publish Online"
2. Review the information in the the confrimation modal
3. Confrim publishing

## Update a published book 

You can update a book this is published online at any stage. 

After selecting the relevant profile: 
1. Select "Publish Again"
2. Review the information in the the confrimation modal
3. Confrim publishing