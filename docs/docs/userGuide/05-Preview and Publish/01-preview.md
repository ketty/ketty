---
title: 'Preview a Book'
---

The **Preview and Publish** page allows you to preview your book and save publishing profiles that can be used for different distribution channels.

## Accessing preview and publish

To access the preview and publishing options for your book:
1. Navigate to the **Producer** page of your book
2. Select "Preview and Publish" from the top navigation bar

## Understanding the interface

The interface consists of two main components:
- Preview panel (main area) showing your rendered book
- Publishing sidebar (right side) with two tabs:
  - "New Preview"
  - "Publishing Profiles"

### Preview panel features

The preview panel provides several viewing controls:
- Single/double-page view toggle
- Zoom controls
- Page navigation
- Sidebar toggle for fuller view

## Creating new previews

The "New Preview" tab allows you to:
1. Select your format:
   - PDF
   - EPUB
   - Web
2. Configure format-specific settings:
   - Page size/dimensions
   - Include or exclude cover
   - Include or exclude certain front matter pages
3. Select the design template

:::note
Cover inclusion is optional for EPUB format and can be excluded if desired. While you can configure EPUB settings, preview is currently only available for PDF and web formats.
:::

## Saving previews

After configuring your settings:
1. Review the preview 
2. Select "Save Publishing Profile"
3. Provide a name for your profile
4. Access saved profiles in the "Publishing Profiles" tab

