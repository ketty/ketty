---
title: 'Chapters and Parts'
---

Ketty's book structure is flexible, allowing you to organise your content through chapters and parts to match your book's needs.

## Create chapters

There are two ways to add chapters to your book from the **Producer** page:

### Write from scratch

Select the "+" button from the left sidebar to create a new empty chapter. You can then start writing your content directly in the editor.

### Upload existing chapters

1. Select the cloud upload icon from the left sidebar
2. Choose a Word docx file from your file browser
3. Your document's formatting and images will be preserved during import

When uploading existing chapters, Ketty maintains your:
- Heading formatting
- Images and their placement
- Basic text styling

## Create parts

Parts help you organise longer works into logical divisions with common themes or subjects. To create a part in your book:

1. Select the three horizontal dots next to any chapter title
2. Choose "Convert to part" from the menu
3. Position the part where needed in your book body
4. Drag and drop relevant chapters into the part

Parts are designed to contain multiple chapters and help create a clear hierarchical structure in your book.

## Organise your content

Once you have created chapters and parts, you can:
- Reorder chapters and parts by dragging and dropping them in the left sidebar
- Move chapters between parts
- Rename chapters and parts by clicking on their titles
- Delete chapters or parts using the three dots menu

## Best practices

- Create a consistent structure using parts for major themes or sections
- Use meaningful titles for chapters and parts to aid navigation
- Consider your table of contents structure when organising content
- Review your book's organisation from the **Preview and Publish** page to see how it appears to readers
