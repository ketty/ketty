---
title: 'Using the Editor'
---

## Editor overview

The Ketty editor is a browser-based word processor that provides a comprehensive set of tools for content creation and formatting. All changes in the editor are saved automatically as you write.

## Formatting toolbar

The editor toolbar provides quick access to common formatting options. Here's what each tool does, from left to right:

### Text formatting

- **Undo/Redo**: Reverse or restore recent changes
- **Text styles**: Choose from Title, Heading 2, Heading 3, and paragraph text
- **Bold**: Emphasise text with stronger weight
- **Italic**: Apply slanted emphasis to text
- **Underline**: Add an underline to text
- **Inline code**: Format text as code with monospace font

### Block formatting

- **Blockquote**: Indent text to show quoted content
- **Ordered lists**: Create numbered lists (up to two levels)
- **Unordered lists**: Create bulleted lists (up to two levels)
- **Reduce indent**: Decrease indentation of text or list items

### Advanced tools

- **Hyperlink**: Add links to external content (include 'https://' for valid EPUBs)
- **Special characters**: Insert symbols and special characters
- **Find and replace**: Search and modify text with case sensitivity options
- **Full-screen mode**: Toggle distraction-free writing

## Keyboard shortcuts

Speed up your writing with these keyboard shortcuts:

### Text editing

- Undo: `Ctrl` + `z`
- Redo: `Ctrl` + `Shift` + `z`
- Save changes: `Ctrl` + `s`

### Text formatting

- Bold: `Ctrl` + `b`
- Italics: `Ctrl` + `i`
- Underline: `Ctrl` + `u`

### Lists and indentation

- Create numbered list: `Shift` + `Ctrl` + `9`
- Create bulleted list: `Shift` + `Ctrl` + `8`
- Reduce list indent: `Ctrl` + `[`
- Increase list indent: `Ctrl` + `]`

## Working with lists

To create nested lists:
1. Start a new list item
2. Select the item you want to nest
3. Click the list button again to indent
4. Use the reduce indent button to return to the previous level

## Text search and replace

The find and replace tool offers several features:
- Case-sensitive search options
- Navigate between results with next/previous arrows
- Replace individual instances or all occurrences
- Highlight matching text in green

## Writing tips

- Use consistent heading levels for proper document structure
- Take advantage of full-screen mode for focused writing
- Save time with keyboard shortcuts for common formatting tasks
- Check the layout of your book as often as you need to by going to the **Preview and Publish** page
