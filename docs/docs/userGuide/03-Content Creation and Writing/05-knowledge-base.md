---
title: 'Knowledge Base'
---

## Understanding the Knowledge Base

Ketty's Knowledge Base (KB) enhances AI-assisted writing by providing context from your reference materials. This feature allows you to:
- Upload reference documents
- Query your documents while writing
- Get AI responses based on your specific content

## Enable the Knowledge Base

The Book Owner can enable the KB feature:

1. Go to **Book Settings** from the **Producer** page
2. Turn on the 'Knowledge Base' setting
3. Access the KB from the top navigation bar

## Managing KB content

### Add files

1. Select 'Add files' or drag and drop into the files list
2. Choose 'Upload all' or upload individual files

:::tip
You may continue working in your book while the upload completes.
:::

### Remove files

- Select multiple files and click 'Delete'
- Remove individual files using the delete icon
- Confirm deletion when prompted

## Querying the Knowledge Base

To use the KB while writing:

1. Select relevant text in your chapter
2. Click the AI assistant tool
3. Edit or add to your selected text in the 'Content' tab (optional)
4. Type your query and send

:::tip
The KB search is enabled by default when KB functionality is enabled in **Book Settings**. You can disable it for *specific* queries using the 'Ask Knowledge Base' toggle next to the send icon.
:::

## Working with responses

KB responses can include three tabs:
1. **Content**: The AI-generated response
2. **Citations**: References from searched files 
3. **Links**: Relevant links from source materials

:::tip
The insert or copy action applies to the text in the 'Content' tab. If you'd like to include the citations or links in your chapter, first copy this text in the 'Content' tab. 
:::


### Improving responses

You can:
- Edit responses before inserting
- Make follow-up queries
- Combine multiple responses
- Insert sections selectively

## Best practices

1. **Document preparation**
   - Give meaningful titles to the reference materials before uploading
   - Upload relevant reference materials
   - Keep KB content up-to-date
2. **Effective querying**
   - Be specific in your questions
   - Include necessary context
   - Refer to document titles when relevant
   - Review and verify citations
3. **Content integration**
   - Edit responses for flow
   - Verify factual accuracy
   - Maintain consistent style
