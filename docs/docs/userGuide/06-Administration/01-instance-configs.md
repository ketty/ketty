---
title: 'Instance Configurations'
---

## Overview 

To access the **Admin** page, select your initials in the top-right of the navigation bar, then select "Admin".

:::note
An administrator user should be set when a Ketty instance is first deployed and more admins can be added at any time. Speak to your hosting provider about configuring admin access.
:::

## AI Integration

Ketty supports AI use with: 
1. The AI assistant in a book's editor
2. The built-in Knowledge Base at the book level 
3. The AI Book Designer (Beta). 

### Link an AI key 

1. Enable the "AI integration" toggle
2. Enter the key in the input area

Ketty supports OpenAI API keys. This feature can be enabled or disabled at any time.

## Publishing options

Admins can control the publshing options that are avialable to users. 

### Enable formats

Give users access to preview, export and publish: 

1. PDFs
2. EPUBs
3. Online (web)

### Print-on-demand integration

Ketty can be integrated with print-on-demand suppliers so that you can order your printed book directly from the application. Currently, Ketty integrates with [Lulu](https://www.lulu.com/), which enables Book Owners to publish, print, and sell their books internationally with Lulu’s Print-on-demand network. This feature can be enabled or disabled at any time.

Eable the "print-on-demand with Lulu" toggle to give Book Owners the ability to sync their Ketty account with their Lulu account.

### Publish online with Flax

Allow Book Owners to publish their books directly online to a unique, automatically generated URL. 

1. Enable the "Publish online with Flax" toggle
2. Optionally allow PDF and EPUB downloads from the website

## Sign-up terms and conditions

Administrators of a Ketty instance may choose to set terms and conditions for its use which users are required to accept on sign-up. By default, the sign-up terms and conditions are blank. To set the custom terms and conditions for the instance, simply input them into the provided editor and select 'Update Terms and Conditions' to save your changes.

