---
title: 'Introduction to Ketty'
---

## What is Ketty?

Ketty is a modern book production system that runs in your web browser. Whether you're a publisher with a large team or an individual author, Ketty provides everything you need to take your book from manuscript to publication.

### The Ketty advantage

Traditional book production often involves multiple tools, complex handoffs, and time-consuming processes. Ketty simplifies this by:

- **Keeping everything in one place**: Write, edit, design, and prepare for publication all within Ketty
- **Making collaboration easy**: Work together with your team
- **Automating technical tasks**: Let Ketty handle formatting and file preparation
- **Providing flexibility**: Choose when to work independently or collaboratively

## Who uses Ketty?

Ketty supports different types of users:

- **Publishers**: Manage multiple books and team members
- **Editors**: Work directly with content and authors
- **Authors**: Write and collaborate with their team
- **Production Staff**: Prepare books for publication
- **Self-Publishers**: Control the entire publishing process

## Core features

### Content creation

- Browser-based writing and editing
- Real-time editing
- Chapter and part management
- AI writing assistance
- Built-in knowledge base

### Team collaboration

- Team member access control
- Chapter-level collaboration
- Commenting tools

### Book production

- Template-based design
- AI-powered book designer
- Multiple export formats (PDF, EPUB)
- Print-on-demand integration
- Publish directly to the web

## Ketty is continuously evolving

Ketty is actively developed with new features and improvements added regularly. We work closely with our community to ensure Ketty meets the evolving needs of publishers and authors.

### Stay connected

Join our community and keep up with the latest developments:

- **[Ketty Forum](https://forum.ketty.community/)**: Discuss features, share experiences, and get help
- **[Development Roadmap](../../roadmap/01-ketty-roadmap.md)**: See what's coming next
- **[Ketty Mattermost Channel](https://mattermost.coko.foundation/coko/channels/ketty)**: Chat with the community
- **email**: Reach us at ketty@coko.foundation

## A note about the name

You may see both "Ketty" and "Ketida" used in reference to this platform. While both names refer to the same system, we've adpoted "Ketty" as the name going forward - a friendly nickname that our community finds easier to pronounce and remember.


