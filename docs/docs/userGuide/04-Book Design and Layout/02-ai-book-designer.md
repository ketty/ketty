---
title: 'AI Book Designer (Beta)'
---

## Overview

The Coko Foundation is leading the way in AI-assisted book design functionality with the beta release of the AI Book Designer in Ketty. The AI Book Designer is a groundbreaking feature that makes PDF design interactive and responsive to natural language commands. This tool allows you to select any element of your document and modify it through simple text instructions.

:::note
As a beta feature, the AI Book Designer can be used as an *alternative* to the design templates. Some prompts may not work as expected. The functionality is continuously being improved and we would value your feedback.
:::

## Accessing the AI Book Designer

The Book Owner can enable this beta feature through the following steps:

1. Navigate to **Book Settings** (cog icon)
2. Turn on the "AI Book Designer (Beta)" setting
3. Access the designer from the top navigation bar

:::note
This feature requires AI integration to be configured at the instance level by an administrator.
:::

## Interface components

The AI Book Designer interface consists of four main elements:

1. **Prompt input**: Where you type your design instructions or questions
2. **Content selector**: Left panel for selecting elements to style
3. **Book PDF preview**: Right panel showing real-time design changes
4. **Chat history**: Record of prompts and responses for each selected element

You can show or hide these components using the checkboxes in the top right of the page.

## Design capabilities

The AI Book Designer supports a wide range of design modifications through natural language prompts:

### Typography

- Font selection and sizing
- Text colour changes
- Style modifications

### Layout

- Page size adjustments
- Text alignment
- Paragraph spacing modifications

### Images

- Size adjustments
- Positioning

### Example prompts

- 'Change all headings to red'
- 'Change the page size to 5 x 8 inches'
- 'Reduce this image by 20 percent'
- 'Center all chapter headings'

## Workflow

1. **Select elements**: Use the content selector to choose what you want to modify
2. **Enter prompt**: Type your design instruction in the prompt input
3. **Review changes**: Check the result in the PDF preview
4. **Iterate**: Adjust your prompts as needed
5. **Export**: When satisfied, export your designed PDF using the printer icon

## Exporting designs

To export your designed book:

1. Review the final design in the PDF preview
2. Click the printer icon in the top right of the preview
3. Save the PDF to your desired location
